from django.contrib import admin
from models import MapTile, Tile, Map, Way, Layer, MapInteractiveText

class AdminTile(admin.ModelAdmin):
    list_display = ['image', 'thumbnail']

    def thumbnail(self, obj):
        return '<img src="/media/%s" width="32" height="32" />' % (obj.image)

    thumbnail.allow_tags = True

class AdminMap(admin.ModelAdmin):
    list_display = ('name', 'created_at')
    list_filter = ('name', 'created_at')


class AdminMapTile(admin.ModelAdmin):
    list_display = ('x', 'y', 'layer', "tile")
    list_filter = ('x', 'y', 'layer', 'tile')


class AdminWay(admin.ModelAdmin):
    pass

class AdminLayer(admin.ModelAdmin):
    pass

class AdminMapInteractiveText(admin.ModelAdmin):
    pass

admin.site.register(Map, AdminMap)
admin.site.register(Tile, AdminTile)
admin.site.register(MapTile, AdminMapTile)
admin.site.register(Way, AdminWay)
admin.site.register(Layer, AdminLayer)
admin.site.register(MapInteractiveText, AdminMapInteractiveText)