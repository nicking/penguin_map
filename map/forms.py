# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.forms import ModelForm
from map.models import Map, Tile, Layer, Way, TileAlbum
import map
from map import models


class CreateMapForm(forms.Form):
    name = forms.CharField(label='Название карты', max_length=70, required=True, widget=forms.TextInput(attrs={'autofocus': 'autofocus'}))
    map_size_x = forms.IntegerField(label='Ширина канваса')
    map_size_y = forms.IntegerField(label='Высота канваса')
    tile_size = forms.IntegerField(label='Размер квадрата')
    text = forms.CharField(label='Описание', initial='', widget = forms.Textarea, required=False)

    # def clean_name(self):
    #     name = self.cleaned_data['name']
    #     name = Map.objects.filter(username=name)
    #     if name:
    #         return ''


class AddPictureForm(ModelForm):
    error_messages = {
        'duplicate_image': (u'Данная картинка уже используется'),
    }

    class Meta:
        model = Tile
        fields = ['image']


class CreateLayer(forms.Form):
    name = forms.CharField(max_length=35, label='Название слоя', required=False)


class AlbumCreateForm(forms.ModelForm):

    class Meta:
        model = TileAlbum
        fields = ['name']