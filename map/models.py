# -*- coding:utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django_pgjson.fields import JsonField
import core
import core.models
from core.models import User


class TileAlbum(models.Model):
    user = models.ForeignKey(User, related_name='tile_albums')
    name = models.CharField(max_length=100, verbose_name='Название')
    image = models.ImageField(verbose_name='Фон альбома', blank=True, upload_to='albums/')

    def save(self, *args, **kwargs):
        for field in self._meta.fields:
            if field.name == 'image':
                field.upload_to = 'album/%d/albums/' % self.user.id
        super(TileAlbum, self).save()

    def delete(self, *args, **kwargs):
        if self.photos:
            for photo in self.photos.all():
                photo.delete()
        storage, path = self.image.storage, self.image.path
        super(TileAlbum, self).delete(*args, **kwargs)
        storage.delete(path)

    def __unicode__(self):
        return self.name


class Tile(models.Model):
    image = models.ImageField(verbose_name='Картинка', upload_to='image-files/')
    tile_album = models.ForeignKey(TileAlbum, related_name='tiles') #TODO мб добавить групповые картинки

    def save(self, *args, **kwargs):
        for field in self._meta.fields:
            if field.name == 'image':
                field.upload_to = 'tiles/%d/%d/' % self.user.id, self.tile_album.id
        super(Tile, self).save()

    def delete(self, *args, **kwargs):
        if self.photos:
            for photo in self.photos.all():
                photo.delete()
        storage, path = self.image.storage, self.image.path
        super(Tile, self).delete(*args, **kwargs)
        storage.delete(path)

    class Meta:
        verbose_name = 'Тайл'
        verbose_name_plural = 'Тайлы'


class Map(models.Model):
    name = models.CharField(max_length=70, verbose_name='Название', unique=True)#TODO Почему blank True?
    text = models.TextField(verbose_name='Описание', blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    map_size_x = models.IntegerField(verbose_name='x')
    map_size_y = models.IntegerField(verbose_name='y')
    tile_size = models.IntegerField(verbose_name='размер квадрата канваса')
    background = models.ImageField(verbose_name='фон', upload_to='map-backgrounds/', blank=True)
    #players_count = models.PositiveSmallIntegerField()
    group_users = models.ForeignKey(core.models.GroupUsers, null=True, blank=True, related_name='maps')
    user = models.ForeignKey(core.models.User, related_name='maps', null=True, blank=True)

    def __unicode__(self):
        return self.name

    def generate_background(self):
        from views.map_drawing import save_background
        save_background(self.id)

    def save(self, *args, **kwargs): #TODO при сохранении создается первый layer можно ли так?)
        super(Map, self).save(*args, **kwargs)
        map = Map.objects.get(id=self.id)
        if not map.layers.filter(z_index=1):
            map.layers.create(z_index=1)

    @property
    def width(self):
        return self.map_size_x * self.tile_size

    @property
    def height(self):
        return self.map_size_y * self.tile_size

    class Meta:
        verbose_name = 'Карта'
        verbose_name_plural = 'Карты'


class Layer(models.Model):
    name = models.CharField(max_length=150, blank=True)
    z_index = models.PositiveSmallIntegerField()
    map = models.ForeignKey(Map, related_name='layers')

    class Meta:
        verbose_name = 'Слой карты'
        verbose_name_plural = 'Слои карты'


class MapInteractiveText(models.Model):
    map = models.ForeignKey(Map, related_name='map_interactive_texts')
    x = models.IntegerField(verbose_name='x')
    y = models.IntegerField(verbose_name='y')
    text = models.TextField(blank=True, verbose_name="дополнительное описание")
    href = models.CharField(max_length=255, blank=True, verbose_name='Ссылка, получение которой происходит при клике на картинку')


class MapTile(models.Model):

    layer = models.ForeignKey(Layer, related_name='map_tiles')
    x = models.IntegerField(verbose_name='x')
    y = models.IntegerField(verbose_name='y')
    # map = models.ForeignKey(Map, related_name='map_tiles')
    tile = models.ForeignKey(Tile, related_name='map_tiles')
    # text = models.TextField(blank=True, verbose_name="дополнительное описание")
    # href = models.CharField(max_length=255, blank=True, verbose_name='Ссылка, получение которой происходит при клике на картинку')

    class Meta:
        verbose_name = 'Тайл карты'
        verbose_name_plural = 'Тайлы карты'


class Way(models.Model):
    map = models.ForeignKey('Map', related_name='ways')
    way = JsonField(help_text=u'Путь миньонов')
    numeric = models.IntegerField(verbose_name='Номер пути', blank=True)
    text = models.TextField(blank=True, verbose_name='Дополнительное описание')

    class Meta:
        verbose_name = 'Путь карты'
        verbose_name_plural = 'Пути карты'