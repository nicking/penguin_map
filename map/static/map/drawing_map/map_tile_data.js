function Map() {
    this.target = 'map';
    this.canvas = undefined;
    this.tile_size = undefined;
    this.map_url = undefined;
    this.data = undefined;
}

Map.prototype.init = function () {
    var self = this;

    var canvas = document.getElementById(self.target);
    var context = canvas.getContext("2d");
    self.canvas = canvas;

    self.context = context;

    $.ajax({
        url: self.map_url,
        dataType: 'json',
        success: function (data) {
            self.data = data; //
            self.tile_size = data.tile_size;

            //canvas.addEventListener("mousedown", self.click.bind(self), false); TODO не забудь удалить ниже
            //canvas.addEventListener("mousemove", self.mousemove_func.bind(self), false);
        }
    });

    setTimeout(function(){
        console.log(self.data);
            if ('interactive_text' in self.data) {
                console.log(self.data);
                canvas.addEventListener("mousedown", self.click.bind(self), false);
                canvas.addEventListener("mousemove", self.mousemove_func.bind(self), false);
            }
        }, 650); // TODO возможно на сервере сделать больший таймаут
};

Map.prototype.click = function (event) {
    var self = this;

    //var x = event.x;
    //var y = event.y;
    //
    //x -= self.canvas.offsetLeft;
    //y -= self.canvas.offsetTop;
    var x = event.offsetX;
    var y = event.offsetY;

    var dx = x - x % self.tile_size;
    var dy = y - y % self.tile_size;

    //получение ссылки по данным
    var data = self.data;
    //(var k = 0; k < self.map_ways.length; k++)

    console.log(data);
    console.log(self);
    for (var i= 0;  i < data['interactive_text'].length; i++){

        console.log(dx/ self.tile_size);
        console.log(dy/ self.tile_size);
        if ((dx / self.tile_size == data['interactive_text'][i].x) && (dy / self.tile_size == data['interactive_text'][i].y)) {
            location.href = data['interactive_text'][i].href;
        }
    }
};


Map.prototype.mousemove_func = function (event) {
    var self = this;

    //var x = event.x;
    //var y = event.y;
    //
    //x -= self.canvas.offsetLeft;
    //y -= self.canvas.offsetTop;
    var x = event.offsetX;
    var y = event.offsetY;

    var dx = x - x % self.tile_size;
    var dy = y - y % self.tile_size;

    var data = self.data;

    var p_text = '';
    for (var i= 0;  i < data['interactive_text'].length; i++){

        if ((dx / self.tile_size == data['interactive_text'][i].x) && (dy / self.tile_size == data['interactive_text'][i].y)) {
            var text = (data['interactive_text'][i].text);
            var href = data['interactive_text'][i].href;
            p_text = '<div class="text-primary">' + 'href=' + href + '</div>'+'<br>' +  'text=' + text;

        }
    }
    $(document).ready(function () {
                $('p').html(p_text);
            });
};