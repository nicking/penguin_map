function Map() {
    this.target = 'map';
    this.canvas = undefined;
    this.map_size_x = undefined;
    this.map_size_y = undefined;
    this.tile_size = undefined;
    this.map_url = undefined;
    this.background_map_interactive_text_url = undefined;
    //this.background_map_interactive_href_url = undefined;

    //urls
    this.background_layer_tile_drawing_url = undefined;
    this.background_tile_add_url = undefined;
    this.background_tile_delete_url = undefined;
    this.swap_layers_url = undefined;
    this.edit_name_layer_url = undefined;
    this.delete_layer_url = undefined;

    this.interactiveTextMapTile = undefined;
    this.deletePictureMode = undefined;
    this.drawImageSrc = undefined;
    this.z_index__list = undefined;
    this.z_index_checkbox_list = [];
    this.z_index = undefined;
    this.check_radio_z_index = undefined;
    this.check_grid = true;
    this.data = undefined;
    this.save_background_url = undefined;
    this.map_ways = undefined;
    this.create_way = 0; //нажатие на клавишу создания пути
    this.moveToWay = undefined; //нужно для создания пути
    this.wayEnd = false;
    this.masWay = undefined; // создаем масив точек в пути
    this.check_way = true;
    this.context = undefined;
    this.z_index_edit_name_layer = undefined;
    this.name_edit_name_layer = undefined;

    this.k = undefined;
}

Map.prototype.init = function () {
    var self = this;

    var canvas = document.getElementById(self.target);
    var context = canvas.getContext("2d");
    self.canvas = canvas;
    canvas.addEventListener("mousedown", self.click.bind(self), false);
    canvas.addEventListener("dblclick", self.dbclick_func.bind(self), false);
    self.context = context;

    $.ajax({
        url: self.map_url,
        dataType: 'json',
        success: function (data) {
            self.tile_size = data.tile_size;
            console.log(data);
            self.data = data; // она мне еще пригодится

            //задаем все данными
            self.map_id = data.map_id; //для других аяксов
            self.z_index__list = data.z_index__list; // список слоев

            self.check_radio_z_index = self.z_index__list.length; // чтоб если рисовали, то сразу на последнем слое TODO уточнить мб -1
            self.z_index_checkbox_list = data.z_index__list;
            //отрисовка путей
            self.map_ways = data.map_ways;

            //отрисовываем все
            self.redrawing_map_for_check_list();
        }
    });
    //рисование картинки
    $(".picture").click(function () {
        self.drawImageSrc = $(this).attr("src");
        self.deletePictureMode = 0;
        self.interactiveTextMapTile = undefined;

    });
    //удаление картинки
    $(".deletePicture").click(function () {
        self.deletePictureMode = 1;
        self.interactiveTextMapTile = undefined;
    });

    //добавление интерактивности для карты
    $(".interactiveTextMapTile").click(function () {
        self.deletePictureMode = undefined;
        self.interactiveTextMapTile = true;
    });

    //получение картинки по выбранным слоям
    $(".save_background").click(function () {
        //если нет выбранных checkbox-ов, то будет сохранение по всему списку слоев
        if (self.z_index_checkbox_list.length > 0) {var z_index_list = self.z_index_checkbox_list}
        else {z_index_list = self.z_index__list}  //TODO var z_index_list не стоит - ни чего стращного?

        $.ajax({
            method: "POST",
            url: self.save_background_url,
            data: {'map_id': self.map_id, 'z_index_list': JSON.stringify(z_index_list) } //TODO self.z_index__list переделать на список list_selected_z_index
        }) //z_index_checkbox_list - z_index_list одно и тоже по логике
            .done(function (mesg) {
                console.log(mesg);
                alert('Сохранено');
            });
    });
    //получение списка из checkbox-ов
    $(".checkbox").click(function () {
        self.z_index = parseInt($(this).attr("name"));
        self.click_checkbox();

        // redrawing map
        self.redrawing_map_for_check_list();
    });

    $(".radio_checkbox").click(function () {
        self.check_radio_z_index = parseInt($(this).attr("z_index"));
        console.log(self.check_radio_z_index)
    });
    // сетка
    $(".checkbox_grid").click(function () {
        if (self.check_grid == true){ self.check_grid = false }
        else {self.check_grid = true;}
        self.redrawing_map_for_check_list();
    });
    //пути
     $(".checkbox_way").click(function () {
        if (self.check_way == true){ self.check_way = false }
        else {self.check_way = true;}
         console.log(self.check_way);
        self.redrawing_map_for_check_list();
    });
    //при выборе двух checkbox - возможность поменять их местами
    $(".swap_layers").click(function () {
        console.log(self.z_index_checkbox_list);
        console.log(self.swap_layers_url);
         $.ajax({
            method: "POST",
            url: self.swap_layers_url,
            data: {'map_id': self.map_id, 'z_index_checkbox_list': JSON.stringify(self.z_index_checkbox_list)}
        }).done(function (msg) {
             location.href = self.map_url;
            });
    });

    // редактирования имени layer
    {
        //сперва запоминаем z_index слоя
        $(".edit_name_layer").click(function () {
            self.z_index_edit_name_layer = parseInt($(this).attr("z_index"));
            self.name_edit_name_layer = parseInt($(this).attr("name")); //получаю Nan TODO уточнить почему (не получается принять str)
            console.log(self.name_edit_name_layer);

            //передаем имя в модальное окошко
            var text = 'z_index=' + self.z_index_edit_name_layer + '' +
                'имя= ' + self.name_edit_name_layer;

            $(document).ready(function () {
                $('about_this_layer').html(text);
            });
        });
        //аякс на изменение имени слоя
        function ajax_name_layer_edit_Func() {
            var new_name = document.getElementById("edit_name_layer_text").value;
            $.ajax({
                method: "POST",
                url: self.edit_name_layer_url,
                data: {'map_id': self.map_id, 'z_index': self.z_index_edit_name_layer, 'new_name': new_name}
            })
                .done(function (mesg) {
                    console.log(mesg);
                    location.href = self.map_url;
                });
        }

        document.getElementById("edit_name_layer_btn").onclick = ajax_name_layer_edit_Func;
    }

    // удаление слоя
    {
        $(".delete_layer").click(function () {
            self.z_index_delete_layer = parseInt($(this).attr("z_index"));
            self.name_delete_layer = parseInt($(this).attr("name")); //получаю Nan TODO уточнить почему (не получается принять str)
            console.log(self.z_index_delete_layer);

            //передаем имя в модальное окошко
            var text = 'z_index=' + self.z_index_delete_layer + '' + '<br>'+
                '   имя= ' + self.name_delete_layer;

            $(document).ready(function () {
                $('about_this_layer').html(text);
            });
        });
        function ajax_delete_layer_Func() {

            $.ajax({
                method: "POST",
                url: self.delete_layer_url,// TODO DELETE
                data: {'map_id': self.map_id, 'z_index': self.z_index_delete_layer}
            })
                .done(function (mesg) {
                    console.log(mesg);
                    location.href = self.map_url;
                });
        }

        document.getElementById("delete_layer_btn").onclick = ajax_delete_layer_Func;
    }
};


Map.prototype.redrawing_map_for_check_list= function () {
    var self = this;
    self.context.clearRect(0, 0, self.canvas.width, self.canvas.height);

    var k = 0;

        //if (self.z_index_checkbox_list.length > 0) {
        //    var refreshId = setInterval(function(){
        //        for (var i=0; i<self.z_index_checkbox_list.length; i++) {
        //            self.z_index = self.z_index_checkbox_list[i];
        //            self.draw_pictures();
        //            k ++ ;
        //            if (k < self.z_index_checkbox_list.length) {
        //                    clearInterval(refreshId);
        //                  }
        //        }
        //    }, 2000 / 2);
            //}

        if (self.z_index_checkbox_list.length > 0) {

            // через рекурсию итерируемся
            var i = -1;
            var timeout = 400;
            var timeout_grid_and_ways = self.z_index_checkbox_list.length * timeout + 100;
            console.log(timeout_grid_and_ways + 100);

            go();
            function go() {
                if (i++ < self.z_index_checkbox_list.length - 1) {
                    self.z_index = self.z_index_checkbox_list[i];
                    self.draw_pictures();
                    setTimeout(go, timeout); //TODO возможность большие таймаут
                }
            }
            //TODO доделать
            //self.k = -1;
            //self.recursive_layer();

        }

    setTimeout(self.marking(), timeout_grid_and_ways);
    setTimeout(self.drawind_ways(), timeout_grid_and_ways);
    //
    ////если чекнута сетка -> ее отрисовываем
    //self.marking();
    ////если чекнут путб -> их отрисовываем
    //self.drawind_ways();
};
//функция выбора нескольких значений в checkbox-е
Map.prototype.click_checkbox= function () {
          //$(this).css('color','#369');
    self = this;
    if (self.z_index_checkbox_list.length > 0) {
        var notElement = true; //исторически :(

        for (var i = 0; i < self.z_index_checkbox_list.length; i++) {
            if ((self.z_index == self.z_index_checkbox_list[i])) {
                console.log('есть');

                //var index = self.z_index_checkbox_list.indexOf(self.z_index); // удаление элемента
                self.z_index_checkbox_list.splice(i, 1);
                notElement = false;
                break;
            }
        }
        if (notElement == true) {
            // нет элемента в списке  -> добавляем его
            self.z_index_checkbox_list[self.z_index_checkbox_list.length] = self.z_index;
            self.z_index_checkbox_list = self.z_index_checkbox_list.sort();
        }
    }
    else {
        //'Первый элемент'
        self.z_index_checkbox_list[self.z_index_checkbox_list.length] = self.z_index;
    }

    if (self.z_index_checkbox_list.length > 0){
        self.z_index = self.z_index_checkbox_list[self.z_index_checkbox_list.length-1]; //если в нашем массиве есть числа, то слой на котором все рисуется
    }
};


//TODO на будущее , хз почему в MOZILE не робит моя обычная рекурсия
//Map.prototype.recursive_layer = function () {
//    self = this;
//    console.log(123);
//                if (self.k++ < self.z_index_checkbox_list.length - 1) {
//                    self.z_index = self.z_index_checkbox_list[self.k];
//                    self.draw_pictures();
//                    setTimeout(self.recursive_layer, 1000); //TODO возможность большие таймаут
//                }
//            };

// отрисовка картинок по layer
Map.prototype.draw_pictures = function () {
    self = this;
    $.ajax({
    method: "POST",
    url: self.background_layer_tile_drawing_url,
    data: {'map_id': self.map_id, 'z_index': self.z_index }
    })
    .done(function (tile_images) {

            console.log(tile_images);
    // wheare your magic here.
            if (tile_images.map_tiles) {
                for (var i = 0; i < tile_images.map_tiles.length; i++) {
                    var image_tile = tile_images.map_tiles[i].image;
                    var x = tile_images.map_tiles[i].x;
                    var y = tile_images.map_tiles[i].y;

                    var dx = x * self.tile_size;
                    var dy = y * self.tile_size;
                    var image = new Image();

                    //image.addEventListener("load", function() {
                    //  // execute drawImage statements here
                    //}, false);
                    //image.src = data[x][y];
                    image.src = image_tile; //self.image;//т.к. идет итерация по списку, и картинку не надо искать
                    self.context.drawImage(image, dx, dy, self.tile_size, self.tile_size);
                }
            }
                });
};
//рисуем сетку
Map.prototype.marking = function () {
    var self = this;
    if (self.check_grid == true) {
        var context = self.canvas.getContext("2d");

        width = self.canvas.width;
        height = self.canvas.height;

        context.strokeStyle = "#EEEEEE";
        context.lineWidth = 1;
        for (var i = 1; i < width/self.tile_size; i++) { //веристкальные линии
            context.beginPath();
            context.moveTo(i * self.tile_size, 0);
            context.lineTo(i * self.tile_size, height);
            context.stroke();
        }
        for (i = 1; i < height/self.tile_size; i++) { //горизонтальные
            context.beginPath();
            context.moveTo(0, i*self.tile_size);
            context.lineTo(width, i*self.tile_size);
            context.stroke();
        }
    }
};

// рисуем пути
Map.prototype.drawind_ways = function () {
    self = this;
    var context = self.context; // TODO МБ КОСЯЧОК
    if (self.check_way == true) {
        if (self.map_ways) {
            self.context.strokeStyle = "#a430c0";
            for (var k = 0; k < self.map_ways.length; k++) {
                for (i = 1; i < self.map_ways[k]['way'].length; i++) {
                    context.beginPath();
                    context.moveTo(self.map_ways[k]['way'][i - 1][0], self.map_ways[k]['way'][i - 1][1]);
                    context.lineTo(self.map_ways[k]['way'][i][0], self.map_ways[k]['way'][i][1]);
                    context.stroke();
                }
            }
        }
    }
};

Map.prototype.click = function (event) {
    var self = this;

    var x = event.offsetX;
    var y = event.offsetY;

    //x -= self.canvas.offsetLeft;
    //y -= self.canvas.offsetTop;

    //offsetX

    var context = self.canvas.getContext("2d");

    dx = x - x % self.tile_size;
    dy = y - y % self.tile_size;

    //console.log('dx=' + dx + 'dy' + dy);
    //
    // var canvas_map = $('#map');
    //
    //    var ballon_x = canvas_map.offset().left,
    //        ballon_y = canvas_map.offset().top;
    //
    //    ballon_x = ballon_x - ballon_x % self.tile_size;
    //    ballon_x = ballon_x - ballon_x % self.tile_size;
    //
    //console.log('dx=' + ballon_x + 'dy' + ballon_x);

    //удаление картинки
    if (self.deletePictureMode == 1) {
        context.fillStyle = "orange";
        context.fillRect(dx, dy, self.tile_size, self.tile_size);
        $.ajax({
            method: "POST",
            url: self.background_tile_delete_url,
            data: {'x': dx / self.tile_size, 'y': dy / self.tile_size, 'map_id': self.map_id, 'z_index': self.check_radio_z_index}
        })
            .done(function (mesg) {
                console.log(dx, dy, mesg);
            });
    }
    // рисование картинки
    else if ((self.deletePictureMode == 0)) {
        var image = new Image();
        image.src = self.drawImageSrc;
        context.drawImage(image, dx, dy, self.tile_size, self.tile_size);
        $.ajax({
            method: "POST",
            url: self.background_tile_add_url,
            data: {'x': dx / self.tile_size, 'y': dy / self.tile_size, 'src': self.drawImageSrc, 'map_id': self.map_id,
                'z_index': self.check_radio_z_index}
        })
            .done(function (mesg) {
                console.log(dx, dy, mesg);
            });
    }

    // добавление описания для тайла последнего слоя
    if (self.interactiveTextMapTile == true ){

        //вызов модального окна
        $("#modalTextMapTile").modal();

        //отправляю данные в окошко
        var text = '<div >'+'dx= '+dx / self.tile_size + '  dy= '+dy / self.tile_size + '</div>' ;

            $(document).ready(function () {
                $('about_this').html(text);
            });

        function ajax_name_layer_edit_Func() {
            var href = document.getElementById("modalTextMapTileHref").value;
            var text = document.getElementById("modalTextMapTileText").value;
            $.ajax({
            method: "POST",
            url: self.background_map_interactive_text_url,
            data: {'x': dx / self.tile_size, 'y': dy / self.tile_size, 'map_id':self.map_id,
                'text': text, 'href': href }
        })
            .done(function (mesg) {
                console.log(dx, dy, mesg);
                    location.href = self.map_url;
            });
        }

        document.getElementById("btn_send_href_and_text_for_interactive_map").onclick = ajax_name_layer_edit_Func;
        }
        //$.ajax({
        //    method: "POST",
        //    url: self.background_map_interactive_text_url,
        //    data: {'x': dx / self.tile_size, 'y': dy / self.tile_size, 'map_id':self.map_id,
        //        'text': 'ПОлучаю текст', 'href': 'Ссылка' }
        //})
        //    .done(function (mesg) {
        //        console.log(dx, dy, mesg);
        //    });
        //}
};


Map.prototype.dbclick_func = function (event) {
    var self = this;

    //var x = event.x;
    //var y = event.y;
    //
    //x -= self.canvas.offsetLeft;
    //y -= self.canvas.offsetTop;
    var x = event.offsetX;
    var y = event.offsetY;

    var dx = x - x % self.tile_size;
    var dy = y - y % self.tile_size;

    console.log('dbclick');

     $("#modalTextMapTile").modal();

        //отправляю данные в окошко
        var text = '<div >'+'dx= '+dx / self.tile_size + '  dy= '+dy / self.tile_size + '</div>' ;

            $(document).ready(function () {
                $('about_this').html(text);
            });

        function ajax_name_layer_edit_Func() {
            var href = document.getElementById("modalTextMapTileHref").value;
            var text = document.getElementById("modalTextMapTileText").value;
            $.ajax({
            method: "POST",
            url: self.background_map_interactive_text_url,
            data: {'x': dx / self.tile_size, 'y': dy / self.tile_size, 'map_id':self.map_id,
                'text': text, 'href': href }
        })
            .done(function (mesg) {
                console.log(dx, dy, mesg);
                    location.href = self.map_url;
            });
        }
        document.getElementById("btn_send_href_and_text_for_interactive_map").onclick = ajax_name_layer_edit_Func;
        };

    //var data = self.data;
    //for (var i= 0;  i < data['interactive_text'].length; i++){
    //
    //    if ((dx / self.tile_size == data['interactive_text'][i].x) && (dy / self.tile_size == data['interactive_text'][i].y)) {
    //        var text = (data['interactive_text'][i].text);
    //        var href = data['interactive_text'][i].href;
    //        p_text = 'href=' + href + 'text=' + text
    //    }
    //    else {
    //        p_text = ''
    //    }
    //}
    //$(document).ready(function () {
    //    $('p').html(p_text);
    //});
