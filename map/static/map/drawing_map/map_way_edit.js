function Map() {
    this.target = 'map';
    this.canvas = undefined;
    this.map_size_x = undefined;
    this.map_size_y = undefined;
    this.tile_size = undefined;
    this.map_url = undefined;
    this.ways_add_url = undefined;
    this.deletePictureMode = undefined;
    this.drawImageSrc = undefined;
    this.map_id = undefined;
    this.data = undefined;
    this.this_url = undefined;

    this.create_way = 0; //нажатие на клавишу создания пути
    this.moveToWay = undefined; //нужно для создания пути
    this.wayEnd = false;
    this.masWay = undefined; // создаем масив точек в пути
}


Map.prototype.init = function () {
    var self = this;

    var canvas = document.getElementById(self.target);
    var context = canvas.getContext("2d");
    self.context = context;
    self.canvas = canvas;
    canvas.addEventListener("mousedown", self.click.bind(self), false);

        $.ajax({
            url: map.map_url,
            dataType: 'json',
            success: function (data) {
                self.tile_size = data.tile_size;
                self.data = data; // она мне еще пригодится

                self.map_id = data.map_id; //для других аяксов

                self.grid();
                self.marking(); // отрисовка сетки
            }

        });

    //рисование пути
    $(".way").click(function () {
        if (self.create_way == 1) { //если второй раз нажата кнопка создать
            self.wayEnd = true;
        }
        else { //первый раз нажата кнопка
            self.masWay = [];
            self.wayEnd = false;
            self.create_way = 1;
            self.deletePictureMode = 10;
            console.log('Сейчас ты можешь рисовать');
        }
    });

    //
    $(".way_numeric").hover(function () {
            $('div').addClass( "selected highlight" );
        var numeric = $(this).attr("numeric");

            for (k = 0; k < self.data.map_ways.length; k++) {
                if (numeric ==self.data.map_ways[k]['numeric']) {
                    var index = k;
                    var data = self.data;
                    context.strokeStyle = "#8B1A1A";
                    context.lineWidth = 4;
                    for (i = 1; i < data.map_ways[index]['way'].length; i++) {
                        context.beginPath();
                        context.moveTo(data.map_ways[index]['way'][i - 1][0], data.map_ways[k]['way'][i - 1][1]);
                        context.lineTo(data.map_ways[index]['way'][i][0], data.map_ways[k]['way'][i][1]);
                        context.stroke();
                    }
                }
            }
    },
        function () {
            self.context.clearRect(0, 0, self.canvas.width, self.canvas.height);
            self.marking();
            context.lineWidth = 1;
            self.grid();

        });
};

// рисуем пути
Map.prototype.grid = function () {
    var self = this;
    var data = self.data;
    var context = self.context;
    //отрисовка путей
    if (data.map_ways) {
        context.strokeStyle = '#00B2EE';
        context.lineWidth = 2;
        for (k = 0; k < data.map_ways.length; k++) {
            for (i = 1; i < data.map_ways[k]['way'].length; i++) {
                context.beginPath();
                context.moveTo(data.map_ways[k]['way'][i - 1][0], data.map_ways[k]['way'][i - 1][1]);
                context.lineTo(data.map_ways[k]['way'][i][0], data.map_ways[k]['way'][i][1]);
                context.stroke();
            }
        }
    }
};
//рисуем сетку
Map.prototype.marking = function () {
    var self = this;
    var context = self.canvas.getContext("2d");

    width = self.canvas.width;
    height = self.canvas.height;

    context.strokeStyle = "#EEEEEE";
    context.lineWidth = 1;
    for (var i = 1; i < width/self.tile_size; i++) { //веристкальные линии
        context.beginPath();
        context.moveTo(i * self.tile_size, 0);
        context.lineTo(i * self.tile_size, height);
        context.stroke();
    }
    for (i = 1; i < height/self.tile_size; i++) { //горизонтальные
        context.beginPath();
        context.moveTo(0, i*self.tile_size);
        context.lineTo(width, i*self.tile_size);
        context.stroke();
    }
};

Map.prototype.click = function (event) {
    var self = this;

    //var x = event.x;
    //var y = event.y;
    //
    //x -= self.canvas.offsetLeft;
    //y -= self.canvas.offsetTop;
    var x = event.offsetX;
    var y = event.offsetY;
    var context = self.canvas.getContext("2d");

    dx = x - x % self.tile_size;
    dy = y - y % self.tile_size;

    //else if((self.deletePictureMode != 0) && ((self.deletePictureMode == 1))){ //TODO лучше использовать это условаие вместо else
     { // идет проверка на нажатие картинки на канвасе или рисовании линий для пути
        var bad_man = ['Чувак, ты не туда попал   ', 'Мне кажется или промазал   ', 'Уууупс, это всего лишь тайл   ',
            'Ойляля, что ты творишь?, подлец  ' +
            'что то не так?', 'Как дела?', 'Все образуется, попробуй еще разок',
            'Я знаю! You can!', 'Еще не надоело?)', 'Я тебе сочувствую'];
        function getRandomBadMan(bad_man) {
            return Math.floor(Math.random() * (bad_man.length)); // рандом для bad_man
        }
        self.bad_man = bad_man[getRandomBadMan(bad_man)];

        // проверка, не нажимаем ли на тайл (проверяя по координатам текущей и из базы данных)
        x =  dx / self.tile_size; y = dy / self.tile_size;
        if  (self.data.map_tiles){
            for (var i = 0; i < self.data.map_tiles.length; i++) {
                //var image_tile = data.map_tiles[i].image;
                if ((x == self.data.map_tiles[i].x) && (y == self.data.map_tiles[i].y)){

                    console.log(bad_man[getRandomBadMan(bad_man)] + self.data.map_tiles[i].image);
                }
            }
        }
        // проводка пути
            if (self.create_way == 1) { //если хотите создать путь
                this.creating_way()
            }

    }
    //// проводка пути
    //if (self.create_way == 1) {
    //    this.creating_way()
    //}
};
// функция создания пути
Map.prototype.creating_way = function() {
    var self = this;
    var context = self.canvas.getContext("2d");

    // проверка на конечную точка (будет конечной, если второй раз нажата кнопка 'нарисовать путь')
    if (self.wayEnd == false) { // если это не конечная точка

        if (self.moveToWay) { //если вдруг у нас уже начальная точка, иначе ее создаем (else ниже)
            //сразу задаем начало от куда рисовать, а конечная будет вычисляться
            context.beginPath();
            context.moveTo(self.moveToWay[0], self.moveToWay[1]);

            // проверка на вертикальные или горизонтальные линии (чтоб не рисовал в диагональ!)
            //проверка на вертикальную  (dx не изменяется а только dy)
            if (dx + self.tile_size / 2 == self.moveToWay[0]){
                //идет проверка выше или ниже (при чем выше со знаком - )
                if (dy + self.tile_size / 2 < self.moveToWay[1]){
                    //то будет прибавляться след точка по y
                    self.moveToWay = [self.moveToWay[0], self.moveToWay[1]-self.tile_size];
                }
                else if (dy + self.tile_size / 2 > self.moveToWay[1]){
                    self.moveToWay = [self.moveToWay[0], self.moveToWay[1]+self.tile_size];
                }
            }
            //проверка на горизонтальную линию (dy не изменяется)
            else if (dy + self.tile_size / 2 == self.moveToWay[1]){
                    console.log('Это  dx  = '+dx);
                    console.log('Это  предыдущ результат  = '+self.moveToWay[0]);

                if (dx > self.moveToWay[0]){
                    //то будет прибавляться след точка по y
                    self.moveToWay = [self.moveToWay[0]+self.tile_size, self.moveToWay[1]];
                }
                //использую else if, чтоб не включать ту же точку
                else if (dx < self.moveToWay[1]){
                    console.log('Я ТАМ ГДЕ НАДО!');
                    self.moveToWay = [self.moveToWay[0]-self.tile_size, self.moveToWay[1]];
                }
            }

            else { console.log(self.bad_man)}


            //context.beginPath();
            //context.moveTo(self.moveToWay[0], self.moveToWay[1]);
            //self.moveToWay = [dx + self.tile_size / 2, dy + self.tile_size / 2];
            context.lineTo(self.moveToWay[0], self.moveToWay[1]);
            context.stroke();

            self.masWay[self.masWay.length] = self.moveToWay;
        }
        else { // первый раз задаем начальную точку рисовании линии
            context.strokeStyle = "#c550c0"; // т.к. до этого переопределял цвет линий
            //self.moveToWay = [dx + self.tile_size / 2, dy + self.tile_size / 2]; // от куда покажешь - там и нарисует (устаревший вариант

            // будет рисовать от края либо х, либо у
            // будем сравнивать, что ближе x или y к краю (если одинаковы - отдаем предпочтение x)
            // логика для x   находим расстояние от середины до точки
            var width_x = (self.canvas.width) / 2; //делим на 2, т.к. с разных сторон идет проверка
            if (dx + self.tile_size / 2 > width_x) {
                x = (dx + self.tile_size / 2) - width_x;
                var top_x = true; // т.е. точка находится ближе к границе справа
            }
            else {
                x = width_x - (dx + self.tile_size / 2)
            }
            // логика для y   (такая же как у X)
            var height_y = (self.canvas.height) / 2; //делим на 2,
            if (dy + self.tile_size / 2 > height_y) {
                y = (dy + self.tile_size / 2) - height_y;
                var top_y = true; //т.е. точка находится ближе к границе снизу
            }
            else {
                y = height_y - (dy + self.tile_size / 2)
            }
            // теперь сравниваем x с y - у кого число больше - тот ближе к границе
            if (x >= y) { //граница x
                if (top_x) {
                    self.moveToWay = [self.canvas.width, dy + self.tile_size / 2]; // начальная точка с топа x начинается
                }
                else {
                    self.moveToWay = [0, dy + self.tile_size / 2]; // начальная точка с топа x начинается
                }
            }
            else { // граница y
                if (top_y) {
                    self.moveToWay = [dx + self.tile_size / 2, self.canvas.height]; // начальная точка с топа x начинается
                }
                else {
                    self.moveToWay = [dx + self.tile_size / 2, 0]; // начальная точка с топа x начинается
                }
            }
            self.masWay[self.masWay.length] = self.moveToWay; //добавляет в конец массива точку

            //и сразу создание второй точки в этом же квадрате, где получили самую первую
            context.beginPath();
            context.moveTo(self.moveToWay[0], self.moveToWay[1]);
            self.moveToWay = [dx + self.tile_size / 2, dy + self.tile_size / 2];
            context.lineTo(self.moveToWay[0], self.moveToWay[1]);
            context.stroke();

            self.masWay[self.masWay.length] = self.moveToWay;
        }
    }
    //аякс на сохранение пути  и создание последней точки
    else {
        // будем создавать конечную точку, которая упирается в границу канвасе
        width_x = (self.canvas.width) / 2; //делим на 2, т.к. с разных сторон идет проверка
            if (dx + self.tile_size / 2 > width_x) {
                x = (dx + self.tile_size / 2) - width_x;
                top_x = true; // т.е. точка находится ближе к границе справа
            }
            else {
                x = width_x - (dx + self.tile_size / 2)
            }

            // логика для y   (такая же как у X)
            height_y = (self.canvas.height) / 2; //делим на 2,
            if (dy + self.tile_size / 2 > height_y) {
                y = (dy + self.tile_size / 2) - height_y;
                top_y = true; //т.е. точка находится ближе к границе снизу
            }
            else {
                y = height_y - (dy + self.tile_size / 2)
            }

            // теперь сравниваем x с y - у кого число больше - тот ближе к границе
            if (x >= y) { //граница x
                if (top_x) {
                    self.moveToWay = [self.canvas.width, dy + self.tile_size / 2]; // конечная точка с топа x
                }
                else {
                    self.moveToWay = [0, dy + self.tile_size / 2]; // конечная точка с начала x
                }
            }
            else { // граница y
                if (top_y) {
                    self.moveToWay = [dx + self.tile_size / 2, self.canvas.height]; // конечная точка с топа x
                }
                else {
                    self.moveToWay = [dx + self.tile_size / 2, 0]; // конечная точка с топа x
                }
            }
        //получили конечную точку, теперь рисуем до нее линию
        context.beginPath();
        context.moveTo(self.masWay[self.masWay.length-1][0], self.masWay[self.masWay.length-1][1]); //берем из массива точек последнюю
        context.lineTo(self.moveToWay[0], self.moveToWay[1]); //линия идет до точки вычисленной чуть выше
        context.stroke();

        self.masWay[self.masWay.length] = self.moveToWay; // добавим точку в массив
        console.log('полученный массив точек' + self.masWay);


        self.moveToWay = null; // анулируем точку перехода, что бы по нашей логике при создании нового пути, могли задать начальную точку
        console.log(self.masWay);
        self.create_way = 0; // показываем, что путь создан, и больше на if не переходи (в функции click используется)z
        $.ajax({
            method: "POST",
            url: self.ways_add_url,
            data: {'map_id': self.map_id, 'way':JSON.stringify(self.masWay)} //TODO Саааша, питон не хочет обрабатывать way
            })
            .done(function (msg) {
                console.log(msg);
                    location.href = self.this_url;
            });
    }
};

//Map.prototype.getTileText = function (event) {
//    var self = this;
//
//    var x = event.x;
//    var y = event.y;
//
//    x -= self.canvas.offsetLeft;
//    y -= self.canvas.offsetTop;
//
//    var dx = x - x % self.tile_size;
//    var dy = y - y % self.tile_size;
//
//    var text = self.data.map_tiles.filter(function(){
//        return self.data['x'] == dx / self.tile_size;
//    }).filter(function(){
//        return self.data['y'] == dy/self.tile_size;
//    });
//
//    $(document).ready(function () {
//        $('p').html(text);
//    });
//};