# -*- coding:utf-8 -*-
from django.conf.urls import patterns, url
import django.contrib.auth.views
from django.views.generic import RedirectView

urlpatterns = patterns('map.views',

    url(r'^$', RedirectView.as_view(url='list', permanent=False), name='search_map'),
    url(r'^list$', 'create_map.map_list_view', name='map_list_view'),

    url(r'^create_map/$', 'create_map.create_map', name='create_map'),
     url(r'^pictures/$', 'pictures.pictures', name='picures'),
    # url(r'^list/$', 'map_drawing.map_list_view', name='map_list_view'),

    url(r'^edit/(?P<map_id>\d+)/$', 'map_drawing.edit', name='edit_map_base'),

    url(r'^edit/background/background_map_interactive_text/$', 'map_drawing.background_map_interactive_text', name='background_map_interactive_text'),

    # # edit_map_background - emb
    url(r'^edit/(?P<map_id>\d+)/background/$', 'map_drawing.background', name='emb'),
    url(r'^edit/background/drawing/$', 'map_drawing.background_layer_tile_drawing', name='emb_drawing'),

    url(r'^edit/background/swap_layers/$', 'map_drawing.swap_layers', name='emb_swap_layers'),
    url(r'^edit/background/add/$', 'map_drawing.background_tile_add', name='emb_add'),

    url(r'^edit/background/delete/$', 'map_drawing.background_tile_delete', name='emb_delete'),#нам не важен id_map
    url(r'^edit/background/save_background/$', 'map_drawing.save_background', name='emb_save_background'),
    url(r'^edit/background/edit_name_layer/$', 'map_drawing.edit_name_layer', name='emb_edit_name_layer'),
    url(r'^edit/background/delete_layer/$', 'map_drawing.delete_layer', name='emb_delete_layer'),

    url(r'^edit/background/send_map_data_on_differents_urls', 'map_drawing.send_map_data_on_differents_urls',
        name='send_map_data_on_differents_urls'),
    #
    # # edit_map_ways - emw
    url(r'^edit/(?P<map_id>\d+)/ways/$', 'map_drawing.ways', name='emw'),
    url(r'^edit/ways_add/', 'map_drawing.ways_add', name='map_ways'),
)
