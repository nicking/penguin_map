# -*- coding:utf-8 -*-
from django.core.urlresolvers import reverse

from django.shortcuts import render, redirect
from map.forms import CreateMapForm
from map.models import Map, Layer


def create_map(request):
    c = {}
    form = CreateMapForm(request.GET or None)
    user = request.user
    if form.is_valid():
        print form.cleaned_data
        map = Map.objects.create(  #тут чтоб использовать save  заменю на update
            name=form.cleaned_data['name'],
            map_size_x=form.cleaned_data['map_size_x'],
            map_size_y=form.cleaned_data['map_size_y'],
            tile_size=form.cleaned_data['tile_size'],
            text=form.cleaned_data['text']
        )
        map.user = user
        map.save()
        #тут будет вызываться метод map.save()
        #TODO сделал
        # layer = map.layers.create(z_index = 1) #при создании карты сразу создается слой
        return redirect(reverse('map.views.map_drawing.edit', kwargs={'map_id': map.id}))

    c['form_get'] = form
    return render(request, 'map/map/create_map.html', c)


def map_list_view(request):
    c = {}
    user = request.user
    user_maps = user.maps.all()
    map = Map.objects.all().order_by('created_at')
    # map
    c['object_list'] = map
    c['user_maps'] = user_maps
    # c['user_group_maps']
    return render(request, 'map/map/search_map.html', c)