# -*- coding:utf-8 -*-
from json import loads
# from django.core.serializers.json import loads
import os
from django.core.files import File
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt, csrf_protect, requires_csrf_token
from map.forms import CreateLayer
from map.models import Tile, Map, Way
from django.http import JsonResponse
from django.shortcuts import HttpResponse
from PIL import Image


def edit(request, map_id):

    user = request.user
    map = user.maps.get(id=map_id)

    if request.is_ajax():
        interactive_text = []
        for text in map.map_interactive_texts.all():
            interactive_text.append({'text': text.text,
                         'href': text.href,
                         'x': text.x, 'y':text.y
                         })
        data = {
            'tile_size': map.tile_size
        }
        if interactive_text:
            data['interactive_text'] = interactive_text

        print data
        return JsonResponse(data)

    c = {
        'map': map,
        'map_url': reverse('map.views.map_drawing.edit', kwargs={'map_id': map_id}),
        'background_urls': reverse('map.views.map_drawing.background', kwargs={'map_id': map_id}),
        'ways_urls': reverse('map.views.map_drawing.ways', kwargs={'map_id': map_id})
    }
    return render(request, 'map/drawing_map/map_tile_data.html', c)


def background(request, map_id):

    user = request.user
    map = user.maps.get(id=map_id)
    # layers = map.layers.all().extra(order_by = ['z_index']) #TODO уточни меня пожалуйста
    layers = map.layers.all().order_by('z_index')
    z_index__list = [layer.z_index for layer in layers]

    create_layer_form = CreateLayer(request.GET or None)
    if create_layer_form.is_valid():
        name = ''
        if create_layer_form.cleaned_data['name']:
            name = create_layer_form.cleaned_data['name']
        map.layers.create(name=name, z_index=map.layers.all().count()+1)
        return redirect(background, map_id)

    if request.is_ajax():
        ways = []
        for way in map.ways.all():
            ways.append({'way': way.way,
                         'numeric': way.numeric
                         })
        data = {
            'tile_size': map.tile_size, 'map_id': map_id,
            'z_index__list': z_index__list,

        }
        if ways:
            data['map_ways'] = ways

        return JsonResponse(data)

    c = {
        'images': user.tiles.all(),
        'map': map,
        'layers': layers,
        'map_url': reverse('map.views.map_drawing.background', kwargs={'map_id': map_id}),

        #ajax-ы
        'background_map_interactive_text_url': reverse('map.views.map_drawing.background_map_interactive_text'),
        # 'background_map_interactive_href_url': reverse('map.views.map_drawing.background_map_interactive_href'),

        'background_layer_tile_drawing_url': reverse('map.views.map_drawing.background_layer_tile_drawing'),
        'background_tile_add_url': reverse('map.views.map_drawing.background_tile_add'),
        'background_tile_delete_url': reverse('map.views.map_drawing.background_tile_delete'),
        'save_background_url': reverse('map.views.map_drawing.save_background'),
        'swap_layers_url': reverse('map.views.map_drawing.swap_layers'), #TODO
        'edit_name_layer_url': reverse('map.views.map_drawing.edit_name_layer'),
        'delete_layer_url': reverse('map.views.map_drawing.delete_layer'),

        'show_map_url': reverse('map.views.map_drawing.edit', kwargs={'map_id': map_id}),
        'ways_url': reverse('map.views.map_drawing.ways', kwargs={'map_id': map_id}),
        'create_layer_form': create_layer_form
    }
    return render(request, 'map/drawing_map/map_tile_edit.html', c)


def ways(request, map_id):

    user = request.user
    map = user.maps.get(id=map_id)

    if request.is_ajax():

        #получение путей
        ways = []
        for way in map.ways.all():
            ways.append({'way':way.way,
                         'numeric': way.numeric
                         })
        data = {
            'tile_size': map.tile_size, 'map_id': map_id,
        }

        if ways:
            data ['map_ways'] = ways

        return JsonResponse(data)
    else:
        c = {
            'map': map,
            'map_url': reverse('map.views.map_drawing.ways', kwargs={'map_id': map_id}), #TODO я хз
            'ways_add_url': reverse('map.views.map_drawing.ways_add'),
            'show_map_url': reverse('map.views.map_drawing.edit', kwargs={'map_id': map_id}),
            'background_url': reverse('map.views.map_drawing.background', kwargs={'map_id': map_id}),

            'this_url': reverse('map.views.map_drawing.ways', kwargs={'map_id': map_id}) #TODO

        }
        return render(request, 'map/drawing_map/map_way_edit.html', c)


def edit_name_layer(request):
    if request.is_ajax():
        print 'ajax'
        map_id = request.POST['map_id']
        map = Map.objects.get(id=map_id)
        layer = map.layers.get(z_index=request.POST['z_index'])
        layer.name = request.POST['new_name']
        layer.save()
        print 'ddd'
        mesg = 'ВСе гуд'
    else:
        mesg = 'Печалька'
    return HttpResponse(mesg)


def delete_layer(request):
    if request.is_ajax():
        print 'ajax'
        map_id = request.POST['map_id']
        map = Map.objects.get(id=map_id)
        layer_z_index = int(request.POST['z_index'])
        map_id = int(map_id)

        if map.layers.filter(z_index=layer_z_index):
            if map.layers.all().order_by('-z_index')[0].z_index == layer_z_index: #если последний элемент
                print 'еееее, последний элемент был'
                map.layers.get(z_index=layer_z_index).delete()
            else:
                #начинается страшное'
                z_index_list_swap = [layer.z_index for layer in map.layers.all().order_by('z_index')[layer_z_index-1:]]
                if len(z_index_list_swap) > 1:
                    map.layers.get(z_index = z_index_list_swap[0]).delete()
                    for i in range(1, len(z_index_list_swap)): #идет итерация до последнего(включительно) TODO возможно -1 сделать
                        layer = map.layers.get(z_index = z_index_list_swap[i])
                        layer.z_index = z_index_list_swap[i-1]
                        layer.save()
        mesg = 'ВСе гуд'
    else:
        mesg = 'Печалька'
    return HttpResponse(mesg)


def background_map_interactive_text(request):
    if request.is_ajax() and request.method == 'POST':

        print request.POST
        map_id = int(request.POST['map_id'])
        x = int(request.POST['x'])
        y = int(request.POST['y'])
        text = request.POST['text']
        href = request.POST['href']

        map = Map.objects.get(id=map_id)
        print map.map_interactive_texts.filter(x=x).filter(y=y)

        _map = map.map_interactive_texts.filter(x=x).filter(y=y)
        print _map
        if map.map_interactive_texts.filter(x=x).filter(y=y).count() > 0:
            map = map.map_interactive_texts.filter(x=x).filter(y=y)[0]
            map.text = text
            map.href = href
            map.save()
        else:
            map.map_interactive_texts.create(x=x, y=y, text=text, href=href)

        msg = 'posted'
        return HttpResponse(msg)


def background_map_interactive_href(request):
    if request.is_ajax() and request.method == 'POST':

        print request.POST
        map_id = int(request.POST['map_id'])
        x = int(request.POST['x'])
        y = int(request.POST['y'])
        href = request.POST['href']

        map = Map.objects.get(id=map_id)
        if map.map_texts.filter(x=x).filter(y=y).count() > 0:
            map.map_texts.filter(x=x).filter(y=y)[0].href = href

        else:
            map.map_texts.create(x=x, y=y, href=href)

        msg = 'posted'
        return HttpResponse(msg)


def background_layer_tile_drawing(request):
    #отрисовка тайлов по данному layer-у
    #TODO в js должна быть итерация запросов по выбранным layer-ам
    print 'dsds'
    if request.is_ajax() and request.method == 'POST':
        #получение картинок
        map_id = request.POST['map_id']
        map = Map.objects.get(id=map_id)
        layer = map.layers.get(z_index=request.POST['z_index'])
        map_tiles_images = []
        for map_tile in layer.map_tiles.all():
            map_tiles_images.append({
                'x': map_tile.x,
                'y': map_tile.y,
                'image': map_tile.tile.image.url,
            })
        #получение путей

        tile_images = {
            'tile_size': map.tile_size, 'map_id': map_id,
        }
        if map_tiles_images:
            tile_images['map_tiles'] = map_tiles_images
        return JsonResponse(tile_images)


def swap_layers(request):
    if request.is_ajax() and request.method == 'POST':
        map_id = request.POST['map_id']
        map = Map.objects.get(id=map_id)
        z_index_checkbox_list = loads(request.POST['z_index_checkbox_list'])

        if len(z_index_checkbox_list) > 1:
            layer1 = map.layers.get(z_index = z_index_checkbox_list[0])
            for i in range(1, len(z_index_checkbox_list)): #идет итерация до предпоследнего(включительно)
                print i
                layer2 = map.layers.get(z_index = z_index_checkbox_list[i])
                layer1.z_index = z_index_checkbox_list[i]
                layer1.save()
                layer1 = layer2
            # layer1 = map.layers.get(z_index = z_index_checkbox_list[-1])
            layer1.z_index = z_index_checkbox_list[0]
            layer1.save()
            print 'Go redirect pls'
            msg = 'Gj'
            print reverse(background, kwargs={'map_id':map_id})
            return redirect(reverse(background, kwargs={'map_id':map_id}))

        else:
            msg = "не выбрано checkbox"
    else:
        msg = "не выбрано checkbox"
    return HttpResponse(msg)


def background_tile_add(request):
    if request.is_ajax() and request.method == 'POST':
        x = request.POST['x']
        y = request.POST['y']
        map_id = request.POST['map_id']
        map = Map.objects.get(id=map_id)
        print map.layers.all().count()
        layer = map.layers.get(z_index = request.POST['z_index'])

        tile = Tile.objects.get(image=request.POST['src'].replace('/media/', '')) #TODO крутая штука, используй меня почаще
        layer.map_tiles.filter(x=x, y=y).delete()
        layer.map_tiles.create(x=x, y=y, tile=tile)
        msg = 'ok'
    else:
        msg = "errors"
    return HttpResponse(msg)


def background_tile_delete(request):
     if request.is_ajax() and request.method == 'POST':
        map_id = request.POST['map_id']
        map = Map.objects.get(id=map_id)
        layer = map.layers.get(z_index = request.POST['z_index'])
        x = request.POST['x']
        y = request.POST['y']
        layer.map_tiles.filter(x=x, y=y).delete()
        msg = 'ok'
     else:
         msg = 'errors'
     return HttpResponse(msg)


def save_background(request):
    #сюда будет приходить POST с map_id и с списком слоев
    # будет итерация по этому списку
    if request.is_ajax() and request.method == 'POST':
        map = Map.objects.get(id=request.POST['map_id'])
        z_index_list = loads(request.POST['z_index_list']) # должны приходить отсортированные
        map_background= Image.new('RGBA', (map.width, map.height), 'white') # TODO возможно наборот
        size = map.tile_size, map.tile_size #использую для изменения размера вставляемой (чтоб не вычислять размер каждую итерацию)

        for z_index in z_index_list:
            print z_index
            map_tiles = map.layers.get(z_index = z_index).map_tiles.all()

            # map_background = Image.new('RGBA', (map.width, map.height)) # TODO хз а нужно ли это тут
            for map_tile in map_tiles:
                print map_tile

                img = Image.open(map_tile.tile.image.path)

                img = img.resize(size, Image.ANTIALIAS)

                map_background.paste(img, (map_tile.x*map.tile_size, map_tile.y*map.tile_size), img)


        #после двух итераций
        print 'sfsd'
        if map.background:
            map_background.save(map.background.path, quality=90)
        else:
            filename = '%s' % map.id + '.png'
            temp_image = open(os.path.join('/tmp', filename), 'w')
            thumb_file = File(open(os.path.join('/tmp', filename), 'r'))
            map.background.save(filename, thumb_file, save=True)
            map.save()

            map_background.save(map.background.path, quality=90)
    return HttpResponse()


def ways_add(request):
    if request.is_ajax() and request.method == 'POST':
        map = Map.objects.get(id=request.POST['map_id'])
        try:
            numeric = map.ways.all().count() + 1 #получаем новый путь
        except Exception:
            numeric = 1
        way = loads(request.POST['way'])
        print way
        # map.ways.create(way=way, numeric=numeric)
        # map.ways.create(way=way) #numeric добавляется в методе save() модели
        new_way = Way(map=map, way=way, numeric=numeric)
        new_way.save()
        msg = ''
    else:
        msg = u'ВСе печально'
    return HttpResponse(msg)


# @csrf_exempt
# @csrf_protect
# @requires_csrf_token
# @requires_csrf_token
def send_map_data_on_differents_urls(request):

    if request.POST:
        map = Map.objects.get(id=request.POST['map_id'])

        interactive_text = []
        for text in map.map_interactive_texts.all():
            interactive_text.append({'text': text.text,
                         'href': text.href,
                         'x': text.x, 'y':text.y
                         })

        data = {
            'tile_size': map.tile_size,
            'map_backgroun': map.background.url,
            'interactive_text': []
        }
        if interactive_text:
            data['interactive_text'] = interactive_text
        return JsonResponse(data)
    else:
        msg = u'ВСе печально'
    return HttpResponse(msg)
