#-*- coding:utf-8 -*-
from django.core.files import File
from django.contrib.auth.decorators import login_required

from django.shortcuts import render, redirect
from django.views.generic.detail import SingleObjectMixin
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView
from map import forms
from map.models import Tile, TileAlbum
from map.forms import AddPictureForm


class LoginRequiredMixin(object): #TODO для более приятного добавления
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view)


class AlbumsList(SingleObjectMixin, ListView):
    model = TileAlbum
    paginate_by = 15
    template_name = 'map/picture/album_list.html'

    def get_queryset(self):
        return self.request.user.tile_albums.objects.all()

    def get(self, request, *args, **kwargs):
        self.object = super(AlbumsList, self).get_queryset()
        return super(AlbumsList, self).get(request, *args, **kwargs)

albums_list_view = AlbumsList.as_view()


class AlbumsCreate(CreateView):
    model = TileAlbum
    form = forms.AlbumCreateForm
    template_name = 'map/picture/album_create.html'


def tiles(request, album_id):
    c = {}
    user = request.user
    if request.POST:

        for file_ in request.FILES.getlist('files'):
            Tile.objects.create(image=File(file_), user=user)
        return redirect(tiles, album_id)

    c['object_list'] = user.tile_albums.get(id=album_id).tiles.all()
    return render(request, 'map/picture/picture_album.html', c)
