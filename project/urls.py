# -*- coding:utf-8 -*-

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static


import map.urls
import auth2.urls
import auth2.urls
import core.urls

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^map/', include(map.urls)),
    url(r'^auth2/', include(auth2.urls)),
    url(r'', include(core.urls)),
    url(r'^$', 'core.views.index', name='index'),
    url(r'^logout/', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),
    url(r'^grppll/', include('grappelli.urls')),

) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
